# Charter Of Fruitfull Interactions Between Communities And Institutions V2

CC 4.0 BY SA 
Collaborative edition realised during "Thematic school"
>>> (names list pending)
Based on the original document "Guidelines to foster fruitful interactions between hacker/maker communities and institutions" CC 4.0 BY SA  found below https://bit.ly/2RRhcrX
________________

>>> Insert New Chart HERE


____________

Note : Original version  "Guidelines to foster fruitful interactions between hacker/maker communities and institutions" CC 4.0 BY SA 
https://bit.ly/2RRhcrX

Guidelines to foster fruitful interactions between hacker/maker communities and institutions #ESOF2018 #SoftHack  
September 20, 2018
On July 2018, ESOF (Euroscience Open Forum), took place in Toulouse. ESOF is a big interdisciplinary science meeting organized every two years in Europe. It is dedicated to scientific research and innovation and gathers scientists, policymakers, business people, students, journalists. More than plenary conferences, several sessions are organized in parallel according to different tracks (scientific, careers, business programmes).
We proposed a session entitled “Soft hacking Science: Learning from success and failure at the intersection between research and hacker / maker culture”, together with other organizations: HackYourPhD, Epidemium, La Paillasse, DIY Science Podcast, Institut Pasteur, Vulca European Program, RFFLabs.
At the end of the session, we decided to summarize our exchange in the form of guidelines to foster interactions between hacker/maker culture and research institutions. You will find below a first version of this guidelines. Feel free to comment.
 
INTRODUCTION
Today’s research takes place in complex landscapes involving multiple stakeholders and requiring collaboration between diverse actors. Integrating these new and heterogeneous contributors requires a shift in frameworks, to make scientific contributions more distributed, collaborative and truly accessible while preserving its quality and review standards. 
As children of the free and open source software movements, we – sometimes as hackers, academics or scientists – have been implementing open scientific practices, promoting collaboration between structures and disciplines in our everyday work and in our organizations and institutions.
We do not intend to speak for all “hacker communities” (as any total definition of hacking will miss the point), but we feel compelled to share this vision, founded on our experience acquired over several years. We hope these guidelines will promote positive interactions between these different techno-scientific-social worlds and foster fruitful collaborations.
 
WHAT’S IN IT FOR ME? 
 
– For communities: collaborating with institutions can expand your perimeter of action and bring you tacit knowledge, methods, tools and funding, that will afford you greater ambitions and continuity. Even if their timing and inertia can be seen as a limiting, remember their rules, norms and procedures have been built around sustainability and security. Learning to deal with their codes and realities in a context where they are also open to deal with yours can lead to fruitful partnerships. 
 
– For institutions:  taking a “walk on the wild side” may help you to reassess the usual approaches to a problem, lead you beyond recruitment-style citizen science to more effectively embed science in society, or give researchers the opportunity to participate in more open and less expected collaborations. If quality standards, replicability, and time invested seem to be an issue when thinking of collaborating with communities, remember their “Test it / Fail it / Fix it” approach could help you cover a larger surface of ideas, and provide you with feedback and examples from their accumulated experience. Moreover, if digital technologies and other transformations sometimes pose a challenge to your organization, communities can be the ideal risk-taking partner to help you probe new and innovative practices.
 
TAKE-HOME MESSAGES
 
TO COMMUNITIES

I Define and explain your modus operandi for a trusting relationship: 
-Consider working with institutions as partners. You have to be considered as new stakeholders with needs of legal and economic acknowledgment.   
-Consider institutions as gold partners. Messing with one can be a dead end experience for a whole sector to work with yours but also other community on a long period of time. Tip: institutions talks to one others. 
-Accept that the institutions have other ways of organizing, making decisions, accept it as a fact without judging and trying to change it BUT communicate clearly about your differences – do not assume they already understand how your community works, or that you know how they work. 
-Define precisely your role and expected outcomes: Academic partners NEED to understand exactly what you are doing.  “Everything” is no good answer considering their classical industrial partners. Explain how you will control the quality of your productions (ex:  External reference, setting up scientific committee…) if appropriate. 
-Do not compromise your project values. Place your own cursor here. Do not compromise outreach gained through collaboration. 
-Explain the governance of your community. Collaborative management and collective decision making processes are often very different from “classical” governance. Help them to understand that community and people is the priority and run a community is very different from running other organizations. 
-Keep your independence as a priority. Having multiple type or sources of partners/fundings. Tip:  “Be a part of it but don’t belong to it” 

II Formalize and create a legal framework: 
-Formalize your collaboration standards (by partnerships) after openly discussing them within your community. Interrogations are on both sides. Explain your “red lines” to your partner and the reasons behind (it’s better if you have already acquired some confidence from them).  Don’t forget you will (probably) have to make compromises. 
-Ask for a contract/or agreement or propose one. Tip: Some ideas here.   
-Read it and make revisions until acceptable on both partners side (It is common and normal procedure) . Interpretation is probably not at your advantage but your partner is (probably) no evil.  Ask why a clause is present. Tip: Be extra cautious with intellectual property. “Open washing” is around my friend, and legal instruments exist to defend your independence. 
-Work using a Creative Commons Licence. You will engage more partners at the end. Extensive examples of big companies successfully implementing open approaches can be found. Discuss intellectual Property from the beginning and give examples 

III Ask the annoying questions (money!)  to create a sustainable framework: 
Consider financial aspects of the project from the beginning. Bringing money concerns in communities is always an issue. Postponing serious discussions about it can sometime compromise a project.   
Care about financial sustainability. First years strict benevolent motivation are sometime hard to maintain. Burnouts are very common. Think about who is doing the work, who is getting paid, and who benefits. Academic employees often don’t consider that collaborators might not have an existing source of income as they are used to collaborating with other academics. Take time to clarify this and have patience. 
Be careful with volunteer or pro-bono work as you will create expectations that the work can be done for free – even if you can afford this, other might not be able to. Document practices and think about the transition to others and extensively discuss it. 
Maintain proper accounts: as soon as possible hire a professional accountant, it will help your project being consistent and understandable for you (sometime) and your partners. 

IV Experiment, iterate, communicate: 
-Start it small and practical: partnerships and money do not fix everything. Do what you are the best at doing: flexibility and adaptation. 
-Test your own experimental solutions to problems you encounter. 
-Publish you work and feedback 
-Again:  Think about people and community first !  
-Integrate and reassure the hierarchy of institutions their goal of stability and security could be respect: start small with them then after a proof of concept you will be able to formalize it. 
-Use your iterative approach to reflect regularly about your priorities and if your values are respected first: don’t hesitate to change and keep your fluidity 

TO INSTITUTIONS

I Be curious:
-Consider working with communities as partners who needs fundings and are not only a bunch of volunteers. 
-Consider also their specificities: communities background can be very different than yours, this is why you came to them and what can make a good partnership. 
-Consider Communities as gold partners: messing with one can be a bad experience and sometime prevent you from developing other “classical” partnerships or collaborations. Tips: communities talks to one others and are already linked to your organization by friends , relatives as suppliers/clients/prospects/evaluation institutions. 

II Create a legal and economic framework: 
-Communities do not work for free. Consider with them in which ways you will gratify collaboration. Most communities don’t want to be considered as service providers . Dare to be creative together Tip: Investing short ressources + having great expectations on a short period of time is a bad bet. 
-Consider communities collaboration standards. Most communities will want to work open source. If you have intellectual property culture, question it. For example, pharma companies nowadays accept working open source so a “without patent , no development” won’t be consistent.  
-Openly discuss your legal norms and create/adapt a smaller perimeter in which it is possible to work openly. There is always a way to exchange even with risk and juridical department. 

III Define a temporality and a perimeter of action: 
-Define an intervention perimeter to be secure because it is sometimes hard to see how communities work compared to your habits and classical partners. 
-Plan your interactions step by step. I.e. Step 1: Informal partnership (Interventions, meetings) / Step 2: Formalise partnership (co organizing events, small projects) / Step 3: Structure partnership (propose to dedicate collaborators time…). 

IV Create specific interactions for collaboration: 
-Dedicate space, time and regularity to make your partnership evolve. Build your own good practices and iterate. 
-Act positively as you would do with a classical partner: quote their job , ask if collaboration should be publicized and how. Many communities are connected  with juridic help. 
-Be patient, proactive and consider or dedicate a collaborator who has time to provide help (different than supervising), exchange and follow up the partnership.  
 
CONCLUSION
One should keep in mind that categories like “institutions” and “communities of hackers/makers” don’t have hard boundaries, and you might find yourself simultaneously at both ends. Those categories are, however, useful to formalize these guidelines, and even if your organization finds itself somewhere in-between, the exercise of putting these guidelines into practice shall make it clear what works for you. Everybody is encouraged to to comment and propose modifications!
As academics and hackers open more paths for partnerships, and as those mature, with both sides reaching useful outcomes for each other and for society at large, we hope that this division will evolve toward more mixed structures of “hackademicians”. Open communities of students, hackers, associations and small companies can be a tremendous force for major research advances. Let’s start expanding this common ground together!
 
Licensing and attribution
This text is licensed under a Creative Commons Attribution 4.0 International (CC BY 4.0) license. Attribution in random order:
Lucy Paterson, @Lu_cyP: @SHD_Berlin co-organiser & @diyscipodcast co-host  

Marc Fournier: @La Paillasse Co-founder 

Olivier de Fresnoye:  @Epidemium_cc Program coordinator

Clémentine Schilte: Project Manager at @Pasteur_CRT (Center for Translational Research) 

Matei Gheorghiu:  Scientific committee of Réseau Français des FabLabs @fablab_fr

Alexandre Rousselet: Vulca European Mobility Program Coordinator @ProgramVulca

HackYourPhD members: @HackYourPhD 
 
Supporting Materials
 
Previous documents that contain community and/or institutional guidelines for science hacking, citizen science and the likes, to be studied and referenced.
A collection of Citizen Science guidelines and publications – DITO: https://ecsa.citizen-science.net/blog/collection-citizen-science-guidelines-and-publications 
Citizen science at universities: Trends, guidelines and recommendations – François Grey et al: https://www.leru.org/publications/citizen-science-at-universities-trends-guidelines-and-recommendations 
White Paper on Citizen Science for Europe: http://www.socientize.eu/sites/default/files/white-paper_0.pdf 
The Art of Relevance – Nina Simon 
Presentation of Lucy Paterson for ESOF: https://www.youtube.com/watch?v=7SytGvubRz0 
Others ? 
